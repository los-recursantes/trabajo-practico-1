package business;

public class Funciones {
	
	//Devuelve una fila de la matriz
		public static int[] obtenerUnaFila(int[][] matriz,int numerofila) {
			int cantColumnas=matriz.length;
			int[] fila=new int[cantColumnas];
			for (int i=0;i<cantColumnas;i++) {
				fila[i]=matriz[numerofila][i];
			}
			return fila;
		}
		
		//Devuelve una columna de la matriz
		public static int[] obtenerUnaColumna(int[][] matriz,int numerocolumna) {
			int cantFilas=matriz[0].length;
			int[] columna=new int[cantFilas];
			for (int i=0;i<cantFilas;i++) {
				columna[i]=matriz[i][numerocolumna];
			}
			return columna;
		}
		
		//Devuelve la suma de todos los enteros de un arreglo
		public static int sumarArreglo(int[] arreglo) {
			int total=0;
			for(int i=0; i<arreglo.length;i++) {
				total+=arreglo[i];
			}
			return total;
		}
		

}
