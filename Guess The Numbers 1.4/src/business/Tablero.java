package business;

public class Tablero{

	private int cantFilas;
	private int cantColumnas;
	private int [][] matriz;
		
	//Constructor de la clase tablero
	public Tablero(int filas, int columnas) {
		this.cantFilas=filas;
		this.cantColumnas=columnas;
		this.matriz=new int[cantFilas][cantColumnas];
		numGenerator();
	}
	
	//Devuelve la cantidad de filas de la tabla
	public int getFilas() {
		return cantFilas;
	}
	
	//Devuelve la cantidad de columnas de la tabla
	public int getColumnas() {
		return cantColumnas;
	}
	
	//Devuelve el numero de la celda indicada
	public int getNumero(int fila,int columna) {
		return matriz[fila][columna];
	}
	

	//Genera numeros aleatorios y los coloca en la matriz
	public void numGenerator() {
		for(int i=0;i<cantColumnas;i++) {
			for(int j=0;j<cantFilas;j++) {
				int numero = (int) (Math.random()*9);
				matriz[j][i]=numero;
			}
		}
	}
	
	//Devuelve la columna de numeros a obtener (la de la derecha)
	public int[] obtenerColumnaResultados() {
		int resultados[]=new int[cantFilas];
		for (int i=0;i<cantFilas;i++) {
			resultados[i]=Funciones.sumarArreglo(Funciones.obtenerUnaFila(this.matriz,i));
		}
		return resultados;
	}
	
	
	//Devuelve la fila de numeros a obtener (la de abajo)
	public int[] obtenerFilaResultados() {
		int resultados[]=new int[cantColumnas];
		for (int i=0;i<cantColumnas;i++) {
			resultados[i]=Funciones.sumarArreglo(Funciones.obtenerUnaColumna(this.matriz,i));
		}
		return resultados;
	}
		
	//Chequea si la matriz ingresada tiene los mismos resultados que el tablero propio.
	public boolean chequeoResultados(int[][] matriz) {
		boolean correcto=true;
		for(int i=0;i<matriz.length;i++) {
			correcto=correcto & Funciones.sumarArreglo(Funciones.obtenerUnaFila(matriz,i))==
					this.obtenerColumnaResultados()[i]
					& Funciones.sumarArreglo(Funciones.obtenerUnaColumna(matriz,i))==
					this.obtenerFilaResultados()[i];		
		}
		return correcto;
	}


}


