package userInterface;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import business.Tablero;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;

public class UI {
	private int cantColumnasTablero;
	private int cantFilasTablero;  
	private JFrame frmGuessTheNumbers;
	private JPanel panelTablero, panelBotones, panelMensajes;
	private Tablero tableroLogico;
	private JTextField[][] tableroVisual;
	private JButton btnIniciar, btnChequear,btnLimpiar;
	private JLabel[] filaLabels, columnaLabels;
	private JLabel lblMensajeFinal,lblErrorMessage;
	private JMenuBar mbarra;
	private JMenu ayuda, Info;
	private JMenuItem instrucciones, version;
	
	
	//Inicia la aplicacion
	public static void Ejecutar() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UI window = new UI();
					window.frmGuessTheNumbers.setVisible(true);
					window.frmGuessTheNumbers.setResizable(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

		
	//Crea la aplicacion
	public UI() {
		initialize();
	}

		
	//Inicializa todos los objetos visuales de la ventana.
	private void initialize() {
		inicializarTableroLogico(4, 4);
		inicializarArrays();
		inicializarFrame();
		inicializarPaneles();
		inicializarButtons();
		inicializarMensajes();
		inicializarCasillas();
		inicializarLabels();
		crearMenu();
		inicializarMenu();
	}

	
	//Funcion para iniciar (o reiniciar) el juego. La usa el boton de inicio.
	private void iniciarJuego() {
		tableroLogico.numGenerator();
		cargarResultados();
		btnIniciar.setText("Reiniciar");
	}
		
	//Carga los resultados a encontrar en los labels
	private void cargarResultados() {
		for(int i=0;i<filaLabels.length;i++) {
			filaLabels[i].setText(""+this.tableroLogico.obtenerFilaResultados()[i]);
			columnaLabels[i].setText(""+this.tableroLogico.obtenerColumnaResultados()[i]);
		}
	}
		
	//Crea el tablero logico (el cual genera los resultados)
	private void inicializarTableroLogico(int filas, int columnas) {
		cantColumnasTablero=filas;
		cantFilasTablero=columnas;
		tableroLogico= new Tablero(cantColumnasTablero,cantFilasTablero);
	}
		
	//Inicializa los arrays que indican a los objetos visuales
	private void inicializarArrays() {
		filaLabels=new JLabel[cantColumnasTablero];
		columnaLabels=new JLabel[cantFilasTablero];
		tableroVisual=new JTextField[cantColumnasTablero][cantFilasTablero];
	}
		
	//Crea el frame principal
	private void inicializarFrame() {
		frmGuessTheNumbers = new JFrame();
		frmGuessTheNumbers.setTitle("Guess the numbers");
		frmGuessTheNumbers.setBounds(100, 100, 450, 320);
		frmGuessTheNumbers.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGuessTheNumbers.getContentPane().setLayout(null);
		frmGuessTheNumbers.getContentPane().setBackground(Color.DARK_GRAY);
	}
		
	
	//a�ade los botones a la barra 
	private void crearMenu(){
		mbarra=new JMenuBar();
		Info=new JMenu("Infomacion");
		ayuda=new JMenu("Ayuda");
		instrucciones=new JMenuItem("Instrucciones");
		version=new JMenuItem("Version");
		ayuda.add(instrucciones);
		Info.add(version);
		mbarra.add(Info);
		mbarra.add(ayuda);
	}
	
	// crea el menu y lo dibuja en pantalla junto con sus acciones
	private void inicializarMenu(){
		frmGuessTheNumbers.setJMenuBar(mbarra);
		mbarra.setBounds(0,0,0,0);
		mbarra.setBackground(Color.LIGHT_GRAY);
		instrucciones.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(panelTablero, "1-La suma de los numeros ingresados debe coincidir con los numeros al final de filas y columnas para ganar \n 2- Si ingresa letras o numeros negativos el tablero los eliminara al chequear el resultado!\n 3- No debe dejar el tablero Incompleto\n 4-Si los resultados no son correctos puede limpiar el tablero (ojo , lo limpia completo)  ","Instrucciones", JOptionPane.OK_OPTION, null);
			}
		});
			version.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					JOptionPane.showMessageDialog(panelTablero, "Este es nuestro primer trabajo practico de programacion III \n Version 1.4\n Los recursantes ");
				}
			});
		}

	
	//Crea los paneles donde se inserta el resto de objetos visuales
	private void inicializarPaneles() {	
		panelBotones = new JPanel();
		panelBotones.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelBotones.setBackground(Color.PINK);
		panelBotones.setBounds(245, 16,168, 122);
		frmGuessTheNumbers.getContentPane().add(panelBotones);
		panelBotones.setLayout(null);
		
		panelMensajes = new JPanel();
		panelMensajes.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelMensajes.setBackground(Color.PINK);
		panelMensajes.setBounds(245, 120, 168, 100);
		frmGuessTheNumbers.getContentPane().add(panelMensajes);
		panelMensajes.setLayout(null);
		
		panelTablero = new JPanel();
		panelTablero.setBounds(15, 16, 215, 212);
		panelTablero.setBackground(Color.PINK);
		frmGuessTheNumbers.getContentPane().add(panelTablero);
		panelTablero.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelTablero.setLayout(null);
	}
		
	//Crea todas las casillas donde ingresar los numeros
	private void inicializarCasillas() {
		int distancia=33;
		int posY= 15;
		for(int i=0;i<cantFilasTablero;i++) {
			int posX= 15;
			for(int j=0;j<cantColumnasTablero;j++) {
				JTextField textField = new JTextField();
				textField.setHorizontalAlignment(SwingConstants.CENTER);
				textField.setColumns(10);
				textField.setBounds(posX, posY, 30, 30);
				panelTablero.add(textField);
				tableroVisual[i][j]=textField;
				posX+=distancia;
			}
			posY+=distancia;
		}
	}
		
	//inicializa los 2 botones: iniciar (reiniciar) y chequear
	private void inicializarButtons() {
		btnIniciar = new JButton("Iniciar");
		btnIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				limpiarTablero();
				limpiarMensajes();
				iniciarJuego();	
				btnChequear.setVisible(true);
			}
		});
		btnIniciar.setBounds(15, 16, 138, 29);
		btnIniciar.setFont(new Font("Trebuchet MS", Font.BOLD, 18));
		panelBotones.add(btnIniciar);	
		
		btnChequear = new JButton("Chequear");
		btnChequear.setVisible(false);
		btnChequear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				limpiarMensajes();
				chequearCasillas();
			}
		});
		btnChequear.setBounds(15, 48, 138, 29);
		btnChequear.setFont(new Font("Trebuchet MS", Font.BOLD, 18));
		panelBotones.add(btnChequear);
			
		btnLimpiar= new JButton("Limpiar Tablero");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0){
				limpiarTablero();
				limpiarMensajes();
			}
		});
		btnLimpiar.setBounds(15, 81, 139, 29);
		btnLimpiar.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
		panelBotones.add(btnLimpiar);
	}

	//Crea los mensajes que se utilizan al chequear las casillas 
	private void inicializarMensajes() {
		lblMensajeFinal = new JLabel("");
		lblMensajeFinal.setFont(new Font("Trebuchet MS", Font.BOLD, 30));
		lblMensajeFinal.setHorizontalAlignment(SwingConstants.CENTER);
		lblMensajeFinal.setBounds(15, 20, 138, 49);
		panelMensajes.add(lblMensajeFinal);
			
		lblErrorMessage = new JLabel("Tablero Incompleto");
		lblErrorMessage.setFont(new Font("Trebuchet MS", Font.BOLD, 15));
		lblErrorMessage.setHorizontalAlignment(SwingConstants.CENTER);
		lblErrorMessage.setBounds(15, 72, 138, 20);
		lblErrorMessage.setVisible(false);
		panelMensajes.add(lblErrorMessage);
	}
		
	//Crea los labels donde van los resultados
	private void inicializarLabels() {
		int posY=15;
		int posX=15;
		int distancia=33;
		for(int i=0;i<cantFilasTablero;i++) {
			JLabel lbl = new JLabel("...");
			lbl.setBounds(158, posY, 30, 30);
			lbl.setHorizontalAlignment(SwingConstants.CENTER);
			lbl.setFont(new Font("Trebuchet MS", Font.BOLD, 20));
			panelTablero.add(lbl);
			columnaLabels[i]=lbl;
			posY+=distancia;
		}
		for(int i=0;i<cantColumnasTablero;i++) {
			JLabel lbl = new JLabel("...");
			lbl.setBounds(posX, 158, 30, 30);
			lbl.setHorizontalAlignment(SwingConstants.CENTER);
			lbl.setFont(new Font("Trebuchet MS", Font.BOLD, 20));
			panelTablero.add(lbl);
			filaLabels[i]=lbl;
			posX+=distancia;
		}
	}
	
	//crea una matriz de int con el contenido de los textfields
	private int[][] convertirTableroVisual() {
		int[][] tableroEnteros=new int[cantFilasTablero][cantColumnasTablero];
		for(int i=0;i<tableroVisual.length;i++) {
			for(int j=0;j<tableroVisual[0].length;j++) {
				tableroEnteros[i][j]=Integer.parseInt(tableroVisual[i][j].getText());
			}
		}
		return tableroEnteros;
	}
	
	//chequea que la casilla tenga un numero positivo (o 0), sino, lo borra y devuelve false
	private boolean validarTextField(int x, int y){
		try {
			if(Integer.parseInt(tableroVisual[x][y].getText())<0) {
				tableroVisual[x][y].setText("");
				return false;
			}
		}
		catch(Exception e) {
			tableroVisual[x][y].setText("");
			return false;
		}
		return true;
	}
	
	//chequea si el tablero es valido o contiene algun caracter invalido
	private boolean validarTablero() {
		boolean valido=true;
		for(int i=0;i<cantFilasTablero;i++) {
			for(int j=0;j<cantColumnasTablero;j++) {
				valido=validarTextField(i, j)&&valido;
			}
		}
		return valido; 
	}
	
	//chequea si los numeros ingresados son correctos
		private void chequearCasillas() {
			if (validarTablero()) {
				if(tableroLogico.chequeoResultados(convertirTableroVisual())) {
					mensajeGanaste();
				}
				else {
					mensajeFallaste();
				}
			}
			else {
				mensajeTableroIncompleto(true);
			}
		}
	
	//funcion que limpia y activa el boton para dejar en balnco el tablero	
	private void limpiarTablero() {
		for(int i=0;i<cantFilasTablero;i++) {
			for(int j=0;j<cantColumnasTablero;j++) {
				tableroVisual[i][j].setText("");
			}
		}
	}
		
	//borra los mensajes de la pantalla
	private void limpiarMensajes(){
		mensajeTableroIncompleto(false);
		lblMensajeFinal.setText("");
	}
		
	//activa y desactiva el mensaje de tableroIncompleto
	private void mensajeTableroIncompleto(boolean estado) {
		this.lblErrorMessage.setVisible(estado);
	}
	
	//activa el mensaje ganaste
	private void mensajeGanaste() {
		this.lblMensajeFinal.setText("Ganaste!");
	}
	
	//activa el mensaje fallaste
	private void mensajeFallaste() {
		this.lblMensajeFinal.setText("Fallaste!");
	}
	
}

