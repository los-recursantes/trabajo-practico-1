package userInterface;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Menu {
	private JFrame frame;
	private JPanel panel;
	private JLabel lblTitle,lblVersion;
	private JButton btnIniciar;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu window = new Menu();
					window.frame.setVisible(true);
					window.frame.setResizable(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Menu() {
		initialize();
	}

	private void initialize() {
		inicializarFrame();
		inicializarPanel();
		inicializarLabels();
		inicializarButton();
	}
	
	private void iniciarJuego() {
		UI.Ejecutar();
		frame.setVisible(false);
		
	}
	
	private void inicializarFrame() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.DARK_GRAY);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
	}
	
	private void inicializarPanel() {
		panel = new JPanel();
		panel.setBackground(Color.PINK);
		panel.setBounds(15, 16, 398, 212);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
	}
	
	private void inicializarLabels() {
		lblTitle = new JLabel("Guess the Numbers");
		lblTitle.setBackground(Color.LIGHT_GRAY);
		lblTitle.setFont(new Font("Gill Sans MT", Font.PLAIN, 38));
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setBounds(15, 16, 368, 49);
		panel.add(lblTitle);
		
		lblVersion = new JLabel("V 1.4");
		lblVersion.setFont(new Font("Segoe UI Historic", Font.PLAIN, 16));
		lblVersion.setHorizontalAlignment(SwingConstants.CENTER);
		lblVersion.setBounds(302, 57, 69, 20);
		panel.add(lblVersion);
	}
	
	private void inicializarButton() {
		btnIniciar = new JButton("Iniciar");
		btnIniciar.setFont(new Font("Trebuchet MS", Font.PLAIN, 24));
		btnIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				iniciarJuego();
			}
		});
		btnIniciar.setBounds(124, 131, 151, 49);
		panel.add(btnIniciar);
	}
}
